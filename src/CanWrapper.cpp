//
// Created by irvy on 8/6/15.
//

#include "CanWrapper.h"


#define INVALID_SOCKET -1

// Simple constructor that makes it "Empty"
CanWrapper::CanWrapper() {

    m_initialzied   = false;
    m_socket        = INVALID_SOCKET;

}

bool CanWrapper::Init(const std::string interface, int &errorCode) {

    struct sockaddr_can addr;
    struct ifreq ifr;

    int ret;

    errorCode = 0;

    m_socket = socket(PF_CAN, SOCK_RAW, CAN_RAW);

    strcpy(ifr.ifr_name,interface.c_str());
    ret = ioctl(m_socket, SIOCGIFINDEX, &ifr);

    // Leaving case.
    // If there is an error, leave
    if(ret){
        errorCode = errno;
        return false;
    }

    addr.can_family     = AF_CAN;
    addr.can_ifindex    = ifr.ifr_ifindex;

    ret = bind(m_socket, (struct sockaddr *)&addr, sizeof(addr));
    if(ret){
        errorCode   = errno;

        close(m_socket);
        m_socket    = INVALID_SOCKET;
        return false;
    }

    m_initialzied   = true;
    return true;
}

void CanWrapper::Close(){
    if(m_initialzied){
        shutdown(m_socket, SHUT_RDWR);
        close(m_socket);

        m_socket        = INVALID_SOCKET;
        m_initialzied   = false;
    }
}

// Sending messages, returning true if it works, false if it fails. Error codes are provided
//
bool CanWrapper::SendMessage(struct can_frame msg, int &errorCode) {
    int res;

    errorCode   = 0;

    if(!m_initialzied) return false;

    res = write(m_socket, &msg, sizeof(struct can_frame));

    if (res < 0){
        errorCode = errno;
        return false;
    }

    return true;
}

void CanWrapper::EnableErrorMessages() {

}

bool CanWrapper::GetMessage(struct can_frame &frame, struct timeval timeout) {
    // We create a brand new socket and read from it.

    int bytesRead;
    int ret;
    fd_set rfds;

    if(!m_initialzied) return false;

    FD_ZERO(&rfds);
    FD_SET(m_socket, &rfds);

    ret = select(m_socket+1, &rfds, NULL, NULL, &timeout);
    if(ret < 0) return false;

    return false;
}

bool CanWrapper::SetRecvBufferSize(int size) {
    return false;
}
