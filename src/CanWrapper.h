//
// Created by irvy on 8/6/15.
//

#ifndef MYIRCOMM_CANWRAPPER_H
#define MYIRCOMM_CANWRAPPER_H

#include <bits/stringfwd.h>

// Includes for linux can stuff
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include "net/if.h"
#include "linux/can.h"
#include "linux/can/raw.h"

/**
 * SocketCan Wrapper that is HEAVELY influenced by this site:
 *          http://support.maximatecc.com/kb/accessing-can-bus-qt-socketcan
 */
class CanWrapper {

public:

    CanWrapper();

    bool Init(const std::string interface, int &errorCode);

    void Close();

    bool SendMessage(struct can_frame msg, int &errorCode );

    bool GetMessage(struct can_frame &frame, struct timeval timeout);

    bool SetRecvBufferSize(int size);

    void EnableErrorMessages();

private:
    bool m_initialzied; // Is the socket on?!

    int m_socket;       // The actual socket
};


#endif //MYIRCOMM_CANWRAPPER_H
